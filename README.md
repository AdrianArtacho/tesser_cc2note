This Patch is part of the [TESSER environment](https://bitbucket.org/AdrianArtacho/tesserakt/src/master/).

![TTESS:Logo](https://bitbucket.org/AdrianArtacho/tesserakt/raw/HEAD/TESSER_logo.png)

# Tesser_CC2note

This device typically takes a CC as input and outputs a midinote.

![repo:TESS:cc2note](https://docs.google.com/drawings/d/e/2PACX-1vS6F8ON1ep3kRFmDo2DAzfIx-Dg7P4KNgODjRyiCb5WxKj1-TA0adGwP_RW8XG8FIHVWnCU7uQSAQAC/pub?w=407&h=390)

## Usage

____

# To-Do

* document, send note off option... etc